#!/usr/bin/env python

import os
from tkinter import Tk
from tkinter import FALSE, N, W, E, S
import tkinter.filedialog
import tkinter.messagebox
from tkinter import ttk

user_name = os.getenv('LOGNAME')
desktop = "/Users/{0}/Desktop".format(user_name)


def run():
    folders = []
    files = []
    disc = tkinter.filedialog.askdirectory()
    archive = open(os.path.join(desktop, 'archive_file'), 'w')
    for r, d, f in os.walk(disc):
        for dir in d:
            folders.append(dir)
        for file in f:
            files.append(file)
    archive.write("------Folders\n")
    for i in folders:
        archive.write(i + '\n')
    archive.write("------Files\n")
    for i in files:
        archive.write(i + '\n')
    archive.close()
    tkinter.messagebox.showinfo(message='Done')

root = Tk()
root.resizable(FALSE, FALSE)
root.title("Archiver")
mainframe = ttk.Frame(root, padding="12 12 12 12")
mainframe.grid(column=0, row=2, sticky=(N, W, E, S))
run_button = ttk.Button(mainframe,
                        text='Run', command=run).grid(column=1, row=2)
instructions = ttk.Label(mainframe,
                         text='Click to make an archive').grid(column=1, row=1)

root.mainloop()
